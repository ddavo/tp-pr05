package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import simulator.misc.Vector;

class VectorTest {

	@Test
	void vectorDot() {
		Vector v1 = new Vector(new double[] {1, 2, 3});
		Vector v2 = new Vector(new double[] {3, 2, 1});
		
		assertEquals(10, v1.dot(v2));
	}

}
