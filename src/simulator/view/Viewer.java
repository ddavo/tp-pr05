package simulator.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import simulator.control.Controller;
import simulator.misc.Vector;
import simulator.model.Body;
import simulator.model.SimulatorObserver;

public class Viewer extends JComponent implements SimulatorObserver {
	private int _centerX;
	private int _centerY;
	private static final int CROSS_RADIUS = 5;
	private static final int PLANET_RADIUS = 5;
	private static final int VECTOR_LENGTH = 20;
	private static final String HELP_STRING = "h: toggle help, +: zoom-in, -: zoom-out, =: fit, v: toggle vector";
	
	private double _scale;
	private List<Body> _bodies;
	private boolean _showHelp;
	private boolean _showVector;
	
	Viewer(Controller ctrl) {
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black, 2),
				"Viewer", TitledBorder.LEFT, TitledBorder.TOP));
		_bodies = new ArrayList<>();
		initGUI();
		ctrl.addObserver(this);
	}
	
	protected void upScale(double factor) {
		// Todo: Calculate maximum _scale factor
		_scale = _scale * factor;
		repaint();
	}
	
	protected void downScale(double factor) {
		_scale = Math.max(1000.0, _scale / factor);
		repaint();
	}
	
	protected class ViewerKeyListener implements KeyListener {
		@Override
		public void keyPressed(KeyEvent e) {
			switch(e.getKeyChar()) {
			case '-':
				upScale(1.1);
				break;
			case '+':
				downScale(1.1);
				break;
			case '=':
				autoScale();
				break;
			case 'h':
				_showHelp = !_showHelp;
				repaint();
				break;
			case 'v':
				_showVector = !_showVector;
				repaint();
				break;
			default:
				break;
			}
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// Nothing to do
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// Nothing to see here
		}
	}
	
	protected class ViewerMouseListener implements MouseListener, MouseWheelListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			if (SwingUtilities.isMiddleMouseButton(e)) {
				autoScale();
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// Nothing to do
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// Nothing to do
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			requestFocus();
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// Nothing to do			
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			int rots = e.getWheelRotation();
			if (rots > 0) {
				for (int i = 0; i < rots; i++) upScale(1.1);
			} else {
				for (int i = 0; i < -rots; i++) downScale(1.1);
			}
		}
		
	}
	
	protected static void drawArrow(Graphics2D gr, int fromx, int fromy, Vector v) {
		int x = (int) (v.direction().coordinate(0)*VECTOR_LENGTH);
		int y = (int) (v.direction().coordinate(1)*VECTOR_LENGTH);
		
		gr.drawLine(fromx, fromy, fromx+x, fromy-y);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D gr = (Graphics2D) g;
		gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		gr.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		_centerX = getWidth() / 2;
		_centerY = getHeight() / 2;
		
		// Drawing bodies
		for (Body b : _bodies) {
			double x = b.getPosition().coordinate(0);
			double y = b.getPosition().coordinate(1);
			if (!Double.isNaN(x) && !Double.isNaN(y)) {
				int scaledx = _centerX + (int)(x/_scale); 
				int scaledy = _centerY - (int) (y/_scale);
				if (_showVector) {
					gr.setStroke(new BasicStroke(2));
					gr.setColor(Color.GREEN);
					drawArrow(gr, scaledx, scaledy, b.getVelocity());
					gr.setColor(Color.YELLOW);
					drawArrow(gr, scaledx, scaledy, b.getAcceleration());
				}
				
				gr.setColor(Color.BLACK);
				gr.drawString(b.getId(), scaledx-gr.getFontMetrics().stringWidth(b.getId())/2, scaledy-2*PLANET_RADIUS);
				gr.setColor(Color.BLUE);
				gr.fillOval(scaledx-PLANET_RADIUS, scaledy-PLANET_RADIUS, PLANET_RADIUS*2, PLANET_RADIUS*2);
			}
		}
		
		gr.setColor(Color.RED);
		
		gr.setStroke(new BasicStroke(1));
		gr.drawLine(_centerX - CROSS_RADIUS, _centerY, _centerX + CROSS_RADIUS, _centerY);
		gr.drawLine(_centerX, _centerY - CROSS_RADIUS, _centerX, _centerY + CROSS_RADIUS);
		
		if(_showHelp) {
			int paddingLeft = getBorder().getBorderInsets(this).left + 5;
			int lineHeight = gr.getFontMetrics().getAscent() + 2;
			int paddingTop = getBorder().getBorderInsets(this).top + lineHeight;
			gr.drawString(HELP_STRING, paddingLeft, paddingTop);
			gr.drawString("Scaling ratio: " + String.valueOf(_scale), paddingLeft, paddingTop + lineHeight);
		}
	}
	
	private void initGUI() {
		_scale = 1.0;
		_showHelp = true;
		_showVector = true;
		
		addKeyListener(new ViewerKeyListener());
		addMouseListener(new ViewerMouseListener());
		addMouseWheelListener(new ViewerMouseListener());
	}
	
	public void autoScale() {
		double max = 1.0;
		
		for (Body b : _bodies) {
			Vector p = b.getPosition();
			for (int i = 0; i < p.dim(); ++i) {
				max = Math.max(max, Math.abs(b.getPosition().coordinate(i)));
			}
		}
		
		double size = Math.max(1.0, Math.min((double) getWidth(), (double) getHeight()));
		_scale = (max>size)?4.0*max/size:1.0;
		repaint();
	}

	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawsDesc) {
		_bodies = bodies;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				autoScale();
			}
		});
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawsDesc) {
		onRegister(bodies, time, dt, gLawsDesc);
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		_bodies = bodies;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				autoScale();
			}
		});
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				repaint();
			}
		});
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
		// The viewer doesn't know the delta time

	}

	@Override
	public void onGravityLawChanged(String gLawsDesc) {
		// The viewer doesn't know the gravity law

	}

}
