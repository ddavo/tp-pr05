package simulator.view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

import simulator.control.Controller;

public class MainWindow extends JFrame {
	private Controller _controller;
	
	public MainWindow(Controller ctrl) {
		super("Physics Simulator");
		_controller = ctrl;
		initGUI();
	}
	
	private void initGUI() {
		// 1. Coloca el panel de control en el PAGE_START del panel mainPanel
		// 2. Coloca la barra de estado en el PAGE_END del mainPanel
		// 3. Crea un nuevo panel que use BoxLayout (y BoxLayout.Y_AXIS)
		// y colócalo en el CENTER de mainPanel. Añade la tabla de cuerpos y el viewer
		// en este panel
		
		// Nota: Aunque JFrame tiene por defecto un BorderLayout, crearemos uno nuevo
		// para poder modificarlo sin tener que castearlo
		
		BorderLayout borderLayout = new BorderLayout(5,5);
		this.setLayout(borderLayout);
		
		StatusBar sb = new StatusBar(_controller);
		this.add(sb, BorderLayout.PAGE_END);
		this.add(new ControlPanel(_controller, sb.getProgressBar()), BorderLayout.PAGE_START);
		
		JComponent tablaDeCuerpos = new BodiesTable(_controller);
		tablaDeCuerpos.setMinimumSize(new Dimension(0,0));
		Viewer viewer = new Viewer(_controller);
		JSplitPane centerPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tablaDeCuerpos, viewer);
		centerPanel.setResizeWeight(0.2);
		// centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		
		this.add(centerPanel, BorderLayout.CENTER);
		
		this.pack();
		this.setMinimumSize(this.getSize());
		viewer.autoScale();
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
}
