package simulator.model;

public class MassLossingBody extends Body {
	private double lossFactor;
	private double lossFrequency;
	private double c;
	
	public MassLossingBody(Body body, double freq, double factor) {
		super(body);
		lossFrequency = freq;
		lossFactor = factor;
	}

	// TODO: Create complex GravityLaws using mass dt
	/**
	 * <p>Nota: Al ir variando la masa con el tiempo, también debería variar su momento/aceleración dependiendo de
	 * las leyes físicas.</p>
	 */
	@Override
	void move(double t) {
		super.move(t);
		c += t;
		if (c >= lossFrequency) {
			c = 0;
			_m = _m*(1-lossFactor);
		}
	}
}
