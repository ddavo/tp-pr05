package simulator.model;

import java.util.List;

import simulator.misc.Vector;

public class NewtonUniversalGravitation implements GravityLaws {
	public static final double G = 6.67E-11;
	
	/** Calcula la fuerza que genera el cuerpo b sobre el cuerpo a */
	private static Vector force(Body a, Body b) {
		double dst = b.getPosition().distanceTo(a.getPosition());
		double k = G * a.getMass() * b.getMass() / (dst * dst);
		return b.getPosition().minus(a.getPosition()).direction().scale(k);
	}

	public void apply(List<Body> bodies) {
		for (Body bi : bodies) {
			int dim = bi.getPosition().dim();
			Vector sum = new Vector(dim);
			for (Body bj : bodies) {
				if (!bi.equals(bj)) {
					sum = sum.plus(force(bi, bj));
				}
			}

			// Sumatorio de fuerzas calculado. F=ma, a = F/m = F.scale(1/m)
			if (bi.getMass() == 0) {
				bi.setAcceleration(new Vector(dim));
				bi.setVelocity(new Vector(dim));
			} else {
				bi.setAcceleration(sum.scale(1.0d / bi.getMass()));
			}
		}
	}
	
	public String toString() {
		return "Newton's law of universal gravitation (nlug)";
	}
}
