package simulator.model;

import java.util.List;

import simulator.misc.Vector;

public class FallingToCenterGravity implements GravityLaws {
	private static final double GRAVITY_ACCELERATION = 9.81;

	@Override
	public void apply(List<Body> bodies) {
		
		for (Body body : bodies) {
			Vector dir = body.getPosition().direction();
			body.setAcceleration(dir.scale(-GRAVITY_ACCELERATION));
		}
	}
	
	public String toString() {
		return "Falling to center gravity (ftcg)";
	}

}
