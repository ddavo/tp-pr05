package simulator.factories;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class BuilderBasedFactory<T> implements Factory<T> {
	/** Una lista de constructores */
	private List<Builder<T>> _builders;
	/** Una lista de objetos JSON construidos por defecto */
	private List<JSONObject> _factoryElements;
	
	public BuilderBasedFactory(List<Builder<T>> list) {
		_builders = new ArrayList<>(list);
		_factoryElements = new ArrayList<>();
		for (Builder<T> builder : _builders)
			_factoryElements.add(builder.getBuilderInfo());
	}

	/** Mediante un bucle de prueba y error buscamos el primer elemento que no devuelva null */
	@Override
	public T createInstance(JSONObject info) {
		T instance = null;
		
		for (int i = 0; instance == null && i < _builders.size(); ++i) {
			instance = _builders.get(i).createInstance(info);
		}
		
		return instance;
	}

	@Override
	public List<JSONObject> getInfo() {
		return new ArrayList<>(_factoryElements);
	}

}
