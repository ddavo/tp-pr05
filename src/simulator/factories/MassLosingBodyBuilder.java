package simulator.factories;

import org.json.JSONObject;

import simulator.model.Body;
import simulator.model.MassLossingBody;

public class MassLosingBodyBuilder extends BasicBodyBuilder {

	public MassLosingBodyBuilder() {
		super("mlb", "Mass Losing Body");
	}
	
	@Override
	protected JSONObject createData() {
		JSONObject data = super.createData();
		data.put("freq", "Mass losing frequency");
		data.put("factor", "0-1 factor of mass loss");
		return data;
	}
	
	@Override
	public Body createTheInstance(JSONObject jo) {
		double freq = jo.getDouble("freq");
		double factor = jo.getDouble("factor");
		
		return new MassLossingBody(super.createTheInstance(jo), freq, factor);
	}
}
