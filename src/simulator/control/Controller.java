package simulator.control;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.json.*;

import simulator.factories.Factory;
import simulator.model.Body;
import simulator.model.GravityLaws;
import simulator.model.PhysicsSimulator;
import simulator.model.SimulatorObserver;

public class Controller {
	private PhysicsSimulator _psim;
	private Factory<Body> _bFac;
	private Factory<GravityLaws> _gFac;
	
	public Controller(PhysicsSimulator pSim, Factory<Body> bFactory, Factory<GravityLaws> gFactory) {
		_psim = pSim;
		_bFac = bFactory;
		_gFac = gFactory;
	}
	
	/** Carga los bodies del json llamando al método addBody.
	 * Asumimos que in contiene una estructura JSON de la forma { "bodies" : [bb1, bb2...] }.
	 * Este método primero transforma la entrada JSON en un objeto JSONObject y luego extrae cada
	 * bb de JSONInput, crea el correspondiente cuerpo b usando la factoria de cuerpos y lo añade al simulador
	 * llamando al metodo addBody
	 *  */
	public void loadBodies(InputStream in) {
		// TODO: Backup if failed
		
		JSONObject jsonInput = new JSONObject(new JSONTokener(in));
		JSONArray jarray = jsonInput.getJSONArray("bodies");
		for (int i = 0; i < jarray.length(); ++i) {
			_psim.addBody(_bFac.createInstance(jarray.getJSONObject(i)));
		}
	}
	
	/**
	 * Ejecuta n pasos del simulador sin escribir nada en consola
	 * @param n
	 */
	public void run(int n) {
		for (int i = 0; i <= n; ++i)
			_psim.advance();
	}
	
	/** Ejecuta el simulador n pasos y muestra los diferentes estados en out usando en formato JSON
	 * <p>Nota: Para que la salida sea como la dada por el profesor, se debe imprimir el estado ANTES de
	 * ejecutar la simulación, y se debe avanzar hasta el último paso</p> */
	public void run(int n, OutputStream out) {
		if (n <= 0) throw new IllegalArgumentException("El número de pasos debe ser mayor que 0");
		PrintStream p = new PrintStream(out);
		
		p.print("{ \"states\" : [ ");
		for (int i = 0; i <= n; ++i) {
			if (i != 0) p.print(", ");
			p.print(_psim.toString());
			_psim.advance();
		}
		p.print(" ] }");
		
		p.close();
	}
	
	/** Invoca al método reset del simulador */
	public void reset() {
		_psim.reset();
	}
	
	/** Invoca al método setDeltaTime del simulador */
	public void setDeltatime(double dt) {
		_psim.setDeltaTime(dt);
	}
	
	/** 
	 * Invoca al método addObserver del simulador
	 * @param o
	 */
	public void addObserver(SimulatorObserver o) {
		_psim.addObserver(o);
	}
	
	/**
	 * Devuelve la factoría de leyes de gravedad
	 * @return
	 */
	public Factory<GravityLaws> getGravityLawsFactory() {
		// TODO: Clone?
		return this._gFac;
	}
	
	public void setGravityLaws(GravityLaws gravityLaws) {
		this._psim.setGravityLaws(gravityLaws);
	}
	
	public void setGravityLaws(JSONObject info) {
		this._psim.setGravityLaws(_gFac.createInstance(info));
	}
}
